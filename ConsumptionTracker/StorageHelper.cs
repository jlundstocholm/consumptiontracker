﻿using System.IO.IsolatedStorage;

namespace ConsumptionTracker
{
    public class StorageHelper
    {
        public static bool ClearPersistent(string key)
        {
            if (null == key)
                return false;

            var store = IsolatedStorageSettings.ApplicationSettings;
            if (store.Contains(key))
                store.Remove(key);
            store.Save();
            return true;
        }

        public static bool SavePersistent(string key, object value)
        {
            if (null == value)
                return false;

            var store = IsolatedStorageSettings.ApplicationSettings;
            if (store.Contains(key))
                store[key] = value;
            else
                store.Add(key, value);

            store.Save();
            return true;
        }

        public static T LoadPersistent<T>(string key)
        {
            var store = IsolatedStorageSettings.ApplicationSettings;
            if (!store.Contains(key))
                return default(T);

            return (T)store[key];
        }
    }
}
