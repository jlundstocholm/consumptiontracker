﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using ConsumptionTracker.Annotations;

namespace ConsumptionTracker.ViewModels
{
    public class MeterReading : INotifyPropertyChanged
    {
        private double _value;

        public double Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public DateTime Time { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
