﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using ConsumptionTracker.Resources;

namespace ConsumptionTracker.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.ElectricityReadings = new ObservableCollection<ElectricityViewModel>();
            this.HeatingReadings = new ObservableCollection<HeatingViewModel>();
            this.WaterReadings = new ObservableCollection<WaterViewModel>();
        }

        /// <summary>
        /// A collection for Electricity reading objects.
        /// </summary>
        public ObservableCollection<ElectricityViewModel> ElectricityReadings { get; private set; }
        public ObservableCollection<HeatingViewModel> HeatingReadings { get; private set; }
        public ObservableCollection<WaterViewModel> WaterReadings { get; private set; }



       

        /// <summary>
        /// Sample property that returns a localized string
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        public void SaveReading(ElectricityViewModel reading)
        {
            this.ElectricityReadings.Add(reading);

            var storedReadings = StorageHelper.LoadPersistent<SortedDictionary<DateTime, double>>("electricity") ?? 
                new SortedDictionary<DateTime, double>();

            storedReadings.Add(reading.Time, reading.Value);
            StorageHelper.SavePersistent("electricity", storedReadings);
        }

        public void SaveReading(HeatingViewModel reading)
        {
            this.HeatingReadings.Add(reading);

            var storedReadings = StorageHelper.LoadPersistent<SortedDictionary<DateTime, double>>("heating") ??
                new SortedDictionary<DateTime, double>();

            storedReadings.Add(reading.Time, reading.Value);
            StorageHelper.SavePersistent("heating", storedReadings);
        }

        public void SaveReading(WaterViewModel reading)
        {
            this.WaterReadings.Add(reading);

            var storedReadings = StorageHelper.LoadPersistent<SortedDictionary<DateTime, double>>("water") ??
                new SortedDictionary<DateTime, double>();

            storedReadings.Add(reading.Time, reading.Value);
            StorageHelper.SavePersistent("water", storedReadings);
        }

        /// <summary>
        /// Clears all meter readings from phone
        /// </summary>
        public void ClearData()
        {
            // Clear from storage
            StorageHelper.ClearPersistent("electricity");
            StorageHelper.ClearPersistent("water");
            StorageHelper.ClearPersistent("heating");

            // Clear from viewmodel
            this.ElectricityReadings.Clear();
            this.HeatingReadings.Clear();
            this.WaterReadings.Clear();
        }

        /// <summary>
        /// Creates and adds objects into the collection.
        /// </summary>
        public void LoadData()
        {
            var electricityReading = StorageHelper.LoadPersistent<SortedDictionary<DateTime, double>>("electricity");

            if (electricityReading != null)
            { 
                foreach (var pair in electricityReading)
                {
                    this.ElectricityReadings.Add(new ElectricityViewModel {Time = pair.Key, Value = pair.Value});
                }
                
            }

            var heatingReading = StorageHelper.LoadPersistent<SortedDictionary<DateTime, double>>("heating");

            if (heatingReading != null)
            {
                foreach (var pair in heatingReading)
                {
                    this.HeatingReadings.Add(new HeatingViewModel { Time = pair.Key, Value = pair.Value });
                }

            }

            var waterReading = StorageHelper.LoadPersistent<SortedDictionary<DateTime, double>>("water");

            if (waterReading != null)
            {
                foreach (var pair in waterReading)
                {
                    this.WaterReadings.Add(new WaterViewModel { Time = pair.Key, Value = pair.Value });
                }

            }
            this.IsDataLoaded = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}