﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsumptionTracker.ViewModels
{
    public class ElectricityViewModel : INotifyPropertyChanged
    {
        private double _value;
        private DateTime _time;

        private const double Epsilon = 0.000001;

        public double Value
        {
            get { return _value; }
            set
            {
                if (Math.Abs(value - _value) > Epsilon)
                {
                    _value = value;
                    OnPropertyChanged();
                }
            }
        }

        public DateTime Time
        {
            get { return _time; }
            set
            {
                if (value != _time)
                {
                    _time = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
