using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ConsumptionTracker.ViewModels
{
    public class WaterViewModel : INotifyPropertyChanged
    {
        private double _value;
        private DateTime _time;

        private const double Epsilon = 0.000001;

        public double Value
        {
            get { return _value; }
            set
            {
                if (Math.Abs(value - _value) > Epsilon)
                {
                    _value = value;
                    OnPropertyChanged();
                }
            }
        }

        public DateTime Time
        {
            get { return _time; }
            set
            {
                if (value != _time)
                {
                    _time = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}