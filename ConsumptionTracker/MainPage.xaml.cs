﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using ConsumptionTracker.ViewModels;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace ConsumptionTracker
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the listbox control to the sample data
            DataContext = App.ViewModel;
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                App.ViewModel.LoadData();
            }
        }

        private void ReadingsSave_Click(object sender, RoutedEventArgs e)
        {

            var currentTime = DateTime.UtcNow;
            var saveCount = 0;

            if (TxtElectricity.Text.Length > 0)
            {
                App.ViewModel.SaveReading(new ElectricityViewModel { Time = currentTime, Value = double.Parse(TxtElectricity.Text) });

                saveCount++;
            }

            if (TxtHeating.Text.Length > 0)
            {
                App.ViewModel.SaveReading(new HeatingViewModel { Time = currentTime, Value = double.Parse(TxtHeating.Text) });
                saveCount++;
            }

            if (TxtWater.Text.Length > 0)
            {
                App.ViewModel.SaveReading(new WaterViewModel { Time = currentTime, Value = double.Parse(TxtWater.Text) });
                saveCount++;
            }

            if (saveCount > 0)
                MessageBox.Show("Readings saved");
        }

        private void ReadingsClear_Click(object sender, RoutedEventArgs e)
        {
            TxtElectricity.Text = "";
            TxtHeating.Text = "";
            TxtWater.Text = "";
        }

        private void ClearAll_onClick(object sender, EventArgs e)
        {
            var result = MessageBox.Show("This will clear your phone for all entered data. Do you wish to continue?",
                "Delete all?", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                   App.ViewModel.ClearData();
            }
        }

        private void SyncToCloud_onClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}